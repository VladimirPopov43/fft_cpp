#include <math.h>
#include <map>
#include <fstream>
#include <iostream>

#include "FastFourierTransform.h"

using namespace std;

static bool TestPairSeries (int dim, const double *p_x, const double *p_y, ostream &output_stm)
{
	if ((dim < 1) || !p_x || !p_y)
		return false;

	NumericUtils::FastFourierTransformer fft_obj (dim);

	if (!fft_obj.IsValidObject ())
		return false;

	double *p_buffer = new double [16*dim];

	double *p_FX = p_buffer, *p_XC = p_FX + dim, *p_XS = p_XC + dim;
	double *p_FY = p_XS + dim, *p_YC = p_FY + dim, *p_YS = p_YC + dim;
	double *p_xr = p_YS + dim, *p_yr = p_xr + dim, *p_FXY = p_yr + dim, *p_covar = p_FXY + dim;
	double *p_Ax = p_covar + dim, *p_Phx = p_Ax + dim, *p_Ay = p_Phx + dim, *p_Phy = p_Ay + dim;
	double *p_Axy = p_Phy + dim, *p_Phxy = p_Axy + dim;

	bool ret_val = fft_obj.CalculateFourierCoefficients (p_x, p_FX) &&
				   fft_obj.CalculateFourierCoefficients (p_y, p_FY) &&
				   fft_obj.BuildFullCosSinCoeffs (p_FX, p_XC, p_XS) &&
				   fft_obj.BuildFullCosSinCoeffs (p_FY, p_YC, p_YS) &&
				   fft_obj.ReverseFourierTransformation (p_FX, p_xr) &&
				   fft_obj.ReverseFourierTransformation (p_FY, p_yr) &&
				   fft_obj.BuildCrossFourierCoefficients (p_FX, p_FY, p_FXY) &&
				   fft_obj.ReverseFourierTransformation (p_FXY, p_covar) &&
				   fft_obj.BuildAmplitudeSpectrum (p_FX, p_Ax, p_Phx) &&
				   fft_obj.BuildAmplitudeSpectrum (p_FY, p_Ay, p_Phy) &&
				   fft_obj.BuildAmplitudeSpectrum (p_FXY, p_Axy, p_Phxy);

	if (ret_val)
	{
		output_stm << "x,y,,FX,XC,XS,,FY,YC,YS,,xr,yr,,FXY,,covar\n";
		for (int cnt=0; cnt<dim; ++cnt)
		{
			output_stm << p_x[cnt] << ',' << p_y[cnt] << ',' << ',' <<
						  p_FX[cnt] << ',' << p_XC[cnt] << ',' << p_XS[cnt] << ',' << ',' <<
						  p_FY[cnt] << ',' << p_YC[cnt] << ',' << p_YS[cnt] << ',' << ',' <<
						  p_xr[cnt] << ',' << p_yr[cnt] << ',' << ',' <<
						  p_FXY[cnt] << ',' << ',' << p_covar[cnt] << endl;
		}

		output_stm << "\nAx,Phx,,Ay,Phy,,Axy,Phxy\n";
		for (int cnt=0; cnt<fft_obj.GetAmplSize(); ++cnt)
		{
			output_stm << p_Ax[cnt] << ',' << p_Phx[cnt] << ',' << ',' <<
						  p_Ay[cnt] << ',' << p_Phy[cnt] << ',' << ',' <<
						  p_Axy[cnt] << ',' << p_Phxy[cnt] << endl;
		}

		double Dx = .0, Dy = .0;
		for (int cnt=0; cnt<dim; ++cnt)
		{
			double diff = p_x[cnt] - p_xr[cnt];
			Dx += diff*diff;

			diff = p_y[cnt] - p_yr[cnt];
			Dy += diff*diff;
		}

		double Ex=.0, Ey = .0, Exy = .0;
		for (int cnt=0; cnt<fft_obj.GetAmplSize(); ++cnt)
		{
			Ex += p_Ax[cnt] *p_Ax[cnt];
			Ey += p_Ay[cnt] *p_Ay[cnt];
			Exy += p_Axy[cnt] *p_Axy[cnt];
		}

		output_stm << "\nDx,,Dy,,IR\n" << Dx << ',' << ',' << Dy << ',' << ',' << Exy / sqrt (Ex*Ey) << endl;
	}

	delete [] p_buffer;

	return true;
}

int main ()
{
/*
	const int dim=72;
	const double A1 = 3., A2 = 1.;

	double x[dim], y[dim];

	for (int cnt=0; cnt<dim; ++cnt)
		x[cnt] = A1*sin (4.*M_PI*cnt/dim + M_PI/3.) + A2*cos (10.*M_PI*cnt/dim - M_PI/4.);

	for (int cnt=0; cnt<9; ++cnt)
		y[cnt] = cnt/9.;
	for (int cnt=9; cnt<27; ++cnt)
		y[cnt] = 2. - cnt/9.;
	for (int cnt=27; cnt<45; ++cnt)
		y[cnt] = cnt/9. - 4.;
	for (int cnt=45; cnt<63; ++cnt)
		y[cnt] = 6. - cnt/9.;
	for (int cnt=63; cnt<72; ++cnt)
		y[cnt] = cnt/9. - 8.;
*/
  	const int dim=72;
	const int A = 53;

	double x[dim], y[dim];

	for (int cnt=0; cnt<dim; ++cnt)
	{
        x[cnt] = ((double)cnt) / ((double)dim);
        y[cnt] = (cnt < A) ? 1. : .0;
	}

	ofstream f_o ("result.csv");

	TestPairSeries (dim, x, y, f_o);

	return 0;
}
